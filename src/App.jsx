import './App.scss';

//react中存在两种形式的组件: 函数式组件, 类组件
//使用 rcc 快速创建 类组件  ( 主要使用这个 )
//使用 rsf 快速创建 函数式组件

import React, { Component } from 'react';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentIndex: 0,
      arr: [
        {
          name: "活动专区",
          shopList: [
            { pic: "https://img0.baidu.com/it/u=1116666846,2210241680&fm=253&fmt=auto&app=138&f=JPEG?w=485&h=500", name: "活动专区1" },
            { pic: "https://img0.baidu.com/it/u=1116666846,2210241680&fm=253&fmt=auto&app=138&f=JPEG?w=485&h=500", name: "活动专区2" },
            { pic: "https://img0.baidu.com/it/u=1116666846,2210241680&fm=253&fmt=auto&app=138&f=JPEG?w=485&h=500", name: "活动专区3" },
            { pic: "https://img0.baidu.com/it/u=1116666846,2210241680&fm=253&fmt=auto&app=138&f=JPEG?w=485&h=500", name: "活动专区4" },
            { pic: "https://img0.baidu.com/it/u=1116666846,2210241680&fm=253&fmt=auto&app=138&f=JPEG?w=485&h=500", name: "活动专区5" },
            { pic: "https://img0.baidu.com/it/u=1116666846,2210241680&fm=253&fmt=auto&app=138&f=JPEG?w=485&h=500", name: "活动专区6" },

          ]
        },
        {
          name: "品牌",
          shopList: [
            { pic: "https://img2.baidu.com/it/u=2301642508,281697021&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "品牌1" },
            { pic: "https://img2.baidu.com/it/u=2301642508,281697021&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "品牌2" },
            { pic: "https://img2.baidu.com/it/u=2301642508,281697021&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "品牌3" },
            { pic: "https://img2.baidu.com/it/u=2301642508,281697021&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "品牌4" },
            { pic: "https://img2.baidu.com/it/u=2301642508,281697021&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "品牌5" },
            { pic: "https://img2.baidu.com/it/u=2301642508,281697021&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "品牌6" },

          ]
        },
        {
          name: "美容护肤",
          shopList: [
            { pic: "https://img0.baidu.com/it/u=3675058468,3248381464&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=501", name: "美容护肤1" },
            { pic: "https://img0.baidu.com/it/u=3675058468,3248381464&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=501", name: "美容护肤2" },
            { pic: "https://img0.baidu.com/it/u=3675058468,3248381464&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=501", name: "美容护肤3" },
            { pic: "https://img0.baidu.com/it/u=3675058468,3248381464&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=501", name: "美容护肤4" },
            { pic: "https://img0.baidu.com/it/u=3675058468,3248381464&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=501", name: "美容护肤5" },
            { pic: "https://img0.baidu.com/it/u=3675058468,3248381464&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=501", name: "美容护肤6" },

          ]
        },
        {
          name: "香水",
          shopList: [
            { pic: "https://img2.baidu.com/it/u=2060204670,276341810&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "香水1" },
            { pic: "https://img2.baidu.com/it/u=2060204670,276341810&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "香水2" },
            { pic: "https://img2.baidu.com/it/u=2060204670,276341810&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "香水3" },
            { pic: "https://img2.baidu.com/it/u=2060204670,276341810&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "香水4" },
            { pic: "https://img2.baidu.com/it/u=2060204670,276341810&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "香水5" },
            { pic: "https://img2.baidu.com/it/u=2060204670,276341810&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "香水6" },
          ]
        },
        {
          name: "酒类",
          shopList: [
            { pic: "https://img0.baidu.com/it/u=3014575567,2749714919&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "酒类1" },
            { pic: "https://img0.baidu.com/it/u=3014575567,2749714919&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "酒类2" },
            { pic: "https://img0.baidu.com/it/u=3014575567,2749714919&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "酒类3" },
            { pic: "https://img0.baidu.com/it/u=3014575567,2749714919&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "酒类4" },
            { pic: "https://img0.baidu.com/it/u=3014575567,2749714919&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "酒类5" },
            { pic: "https://img0.baidu.com/it/u=3014575567,2749714919&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "酒类6" },
          ]
        },
        {
          name: "腕表首饰",
          shopList: [
            { pic: "https://img1.baidu.com/it/u=2740542632,1604771347&fm=253&fmt=auto&app=138&f=JPEG?w=503&h=500", name: "腕表首饰1" },
            { pic: "https://img1.baidu.com/it/u=2740542632,1604771347&fm=253&fmt=auto&app=138&f=JPEG?w=503&h=500", name: "腕表首饰2" },
            { pic: "https://img1.baidu.com/it/u=2740542632,1604771347&fm=253&fmt=auto&app=138&f=JPEG?w=503&h=500", name: "腕表首饰3" },
            { pic: "https://img1.baidu.com/it/u=2740542632,1604771347&fm=253&fmt=auto&app=138&f=JPEG?w=503&h=500", name: "腕表首饰4" },
            { pic: "https://img1.baidu.com/it/u=2740542632,1604771347&fm=253&fmt=auto&app=138&f=JPEG?w=503&h=500", name: "腕表首饰5" },
            { pic: "https://img1.baidu.com/it/u=2740542632,1604771347&fm=253&fmt=auto&app=138&f=JPEG?w=503&h=500", name: "腕表首饰6" },
          ]
        },
        {
          name: "服饰箱包",
          shopList: [
            { pic: "https://img1.baidu.com/it/u=15699451,3275360344&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "服饰箱包1" },
            { pic: "https://img1.baidu.com/it/u=15699451,3275360344&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "服饰箱包2" },
            { pic: "https://img1.baidu.com/it/u=15699451,3275360344&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "服饰箱包3" },
            { pic: "https://img1.baidu.com/it/u=15699451,3275360344&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "服饰箱包4" },
            { pic: "https://img1.baidu.com/it/u=15699451,3275360344&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "服饰箱包5" },
            { pic: "https://img1.baidu.com/it/u=15699451,3275360344&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "服饰箱包6" },
          ]
        },
        {
          name: "食品保健",
          shopList: [
            { pic: "https://img0.baidu.com/it/u=645505420,1762999555&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "食品保健1" },
            { pic: "https://img0.baidu.com/it/u=645505420,1762999555&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "食品保健2" },
            { pic: "https://img0.baidu.com/it/u=645505420,1762999555&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "食品保健3" },
            { pic: "https://img0.baidu.com/it/u=645505420,1762999555&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "食品保健4" },
            { pic: "https://img0.baidu.com/it/u=645505420,1762999555&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "食品保健5" },
            { pic: "https://img0.baidu.com/it/u=645505420,1762999555&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500", name: "食品保健6" },
          ]
        },
      ],
    }
  }

  handleClick(index) {
    this.setState({
      currentIndex: index,
    })

  }

  render() { //渲染组件模板到视图
    return (
      <div className='app'>
        <div className="left">
          {
            this.state.arr.map((item, index) => {
              return (
                <div className={`btn ${this.state.currentIndex === index ? 'actice' : ''}`} onClick={() => {
                  this.handleClick(index)
                }}>{item.name}</div>
              )
            })
          }
        </div>
        <div className="right">
          {
            this.state.arr[this.state.currentIndex].shopList.map((item, index) => {
              return (
                <div className="shop">
                  <img src={item.pic} alt="" />
                  <div className='name'>{item.name}</div>
                </div>
              )
            })
          }
        </div>
      </div>
    );
  }
}

export default App;